# Creates a VPC from Cloudformation template

This template is based on the [cloudformation template form AWS|https://docs.aws.amazon.com/codebuild/latest/userguide/cloudformation-vpc-template.html]. 

## Commands

### Create the VPC
```
# from this directory
aws cloudformation create-stack \
--stack-name rah-sparx-vpn \
--template-body file://codebuild-vpc-cfn.yaml \
--parameters ParameterKey=EnvironmentName,ParameterValue=rah-environment
```

### Destroy the VPC
```
aws cloudformation delete-stack --stack-name rah-sparx-vpn
```
